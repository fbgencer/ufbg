#ifndef PRINT_UFBG_H
#define PRINT_UFBG_H
#include <string>
#include <iostream>


#define PRINT_UFBG_CODE 0


const static string print_function_table[] =
{
	"print",
	"print_table",
	"printme",
	"printer",
	"printfile",	
};


#endif