#include "include.h"
#include "lexer.h"
#include <fstream>
#include <cstdio>
#include <cstdlib>

void printlexer(){
  for(int i=0,j=0; i<lextok.size(); i++){
   // if(iter %5 == 0) printf("\n");
      if(lextok[i] == WORD || isNUM(lextok[i]) || lextok[i] == STRING)
        printf("{%s '%s'}\n",TYPES_ARRAY[lextok[i]].c_str(),lexstr[j++].c_str());
      else printf("{%s}",TYPES_ARRAY[lextok[i]].c_str());
    }
    printf("\n");
}

int main(){

	string lexerinput;
	lexerinput = "x = -5+(-6.123)*(-2+3);";

	lexer(lexerinput) ?  printlexer() : error("In main, Lexer did not work");
	printf("======================\n");
	for (int i = 0; i < lexint.size(); ++i){
		cout<<lexint[i]<<endl;
		cout<<"------------------\n";
		cout<<lexdouble[i]<<endl;
	}
		
	return 0;
}
